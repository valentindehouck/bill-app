/**
 * @jest-environment jsdom
 */

import { screen, fireEvent, waitFor } from "@testing-library/dom"
import NewBillUI from "../views/NewBillUI.js"
import NewBill from "../containers/NewBill.js"
import { localStorageMock } from "../__mocks__/localStorage.js"
import userEvent from "@testing-library/user-event"
import mockStore from "../__mocks__/store.js"
import { ROUTES, ROUTES_PATH } from "../constants/routes"
import router from "../app/Router.js"
import '@testing-library/jest-dom/extend-expect'

jest.mock("../app/Store", () => mockStore)

describe("Given I am connected as an employee", () => {

  beforeEach(() => {
    Object.defineProperty(window, 'localStorage', { value: localStorageMock })
    window.localStorage.setItem('user', JSON.stringify({ type: 'Employee', email: 'a@a' }))
  })


  describe("When I am on NewBill Page", () => {

    test("Then mail icon in vertical layout should be highlighted", async () => {
      const root = document.createElement("div")
      root.setAttribute("id", "root")
      document.body.append(root)
      router()
      window.onNavigate(ROUTES_PATH.NewBill)

      await waitFor(() => screen.getByTestId('icon-mail'))
      const windowIcon = screen.getByTestId('icon-mail')
      expect(windowIcon.classList).toContain('active-icon')
    })
  })


  describe("When I am on NewBill Page and I try to add a file with an acceptable format", () => {

    test("Then the file is uploaded correctly ", async () => {
      document.body.innerHTML = NewBillUI()

      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname })
      }
      const newBill = new NewBill({
        document,
        onNavigate,
        store: mockStore,
        localStorage: window.localStorage
      })

      const handleChangeFile = jest.spyOn(newBill, 'handleChangeFile')
      const inputFile = screen.getByTestId('file')
      const file = new File(['hello'], 'hello.png', { type: 'image/png' })
      await userEvent.upload(inputFile, file)

      expect(handleChangeFile).toHaveBeenCalled()
      expect(inputFile.files[0]).toEqual(file)
      expect(inputFile.files).toHaveLength(1)
    })
  })


  describe("When I am on NewBill Page and I try to add a file with an unacceptable format", () => {

    test("Then the file is not uploaded and an error appears", async () => {
      document.body.innerHTML = NewBillUI()

      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname })
      }
      const newBill = new NewBill({
        document,
        onNavigate,
        store: mockStore,
        localStorage: window.localStorage
      })

      const handleChangeFile = jest.spyOn(newBill, 'handleChangeFile')
      const inputFile = screen.getByTestId('file')
      const file = new File(['hello'], 'hello.gif', { type: 'image/gif' })
      await userEvent.upload(inputFile, file)

      expect(handleChangeFile).toHaveBeenCalled()
      expect(inputFile.files[0]).toEqual(file)
      expect(screen.getByTestId('errorExtension')).toBeVisible()
      expect(inputFile.value).toBeFalsy()
    })
  })


  describe("When I am on NewBill Page and I submit the form", () => {

    test("Then should call handleSubmit method and I am redirected to Bills page", async () => {
      document.body.innerHTML = NewBillUI()
      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname })
      }
      const newBill = new NewBill({
        document,
        onNavigate,
        store: mockStore,
        localStorage: window.localStorage
      })

      const onNavigateSpy = jest.spyOn(newBill, 'onNavigate')
      const handleSubmitForm = jest.spyOn(newBill, 'handleSubmit')
      const form = screen.getByTestId('form-new-bill')
      fireEvent.submit(form)


      expect(handleSubmitForm).toHaveBeenCalled()
      await waitFor(() => screen.getByText('Mes notes de frais'))
      expect(onNavigateSpy).toHaveBeenCalledWith(ROUTES_PATH['Bills'])
      expect(screen.getByText('Mes notes de frais')).toBeTruthy()
      expect(screen.getByTestId('tbody')).toBeTruthy()
    })

    test("Then fails with 404 message error", async () => {
      jest.spyOn(mockStore, 'bills')
      document.body.innerHTML = NewBillUI()
      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname })
      }
      const newBill = new NewBill({
        document,
        onNavigate,
        store: mockStore,
        localStorage: window.localStorage
      })
      const error404 = new Error('Erreur 404')
      mockStore.bills.mockImplementationOnce(() => {
        return {
          update: () => {
            return Promise.reject(error404)
          }
        }
      })

      const onNavigateSpy = jest.spyOn(newBill, 'onNavigate')
      const handleSubmitForm = jest.spyOn(newBill, 'handleSubmit')
      const form = screen.getByTestId('form-new-bill')
      fireEvent.submit(form)

      expect(handleSubmitForm).toHaveBeenCalled()
      expect(onNavigateSpy).not.toHaveBeenCalled()
    })

    test("Then fails with 500 message error", async () => {
      jest.spyOn(mockStore, 'bills')

      document.body.innerHTML = NewBillUI()
      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname })
      }
      const newBill = new NewBill({
        document,
        onNavigate,
        store: mockStore,
        localStorage: window.localStorage
      })
      const error500 = new Error('Erreur 500')
      mockStore.bills.mockImplementationOnce(() => {
        return {
          update: () => {
            return Promise.reject(error500)
          }
        }
      })

      const onNavigateSpy = jest.spyOn(newBill, 'onNavigate')
      const handleSubmitForm = jest.spyOn(newBill, 'handleSubmit')
      const form = screen.getByTestId('form-new-bill')
      fireEvent.submit(form)

      expect(handleSubmitForm).toHaveBeenCalled()
      expect(onNavigateSpy).not.toHaveBeenCalled()
    })
  })
})
