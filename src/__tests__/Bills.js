/**
 * @jest-environment jsdom
 */

import { screen, waitFor } from "@testing-library/dom"
import BillsUI from "../views/BillsUI.js"
import { bills } from "../fixtures/bills.js"
import { ROUTES, ROUTES_PATH } from "../constants/routes.js"
import { localStorageMock } from "../__mocks__/localStorage.js"
import userEvent from '@testing-library/user-event'
import Bills from '../containers/Bills.js'
import mockStore from "../__mocks__/store"
import router from "../app/Router.js"

jest.mock("../app/Store", () => mockStore)

describe("Given I am connected as an employee", () => {

  beforeEach(() => {
    Object.defineProperty(window, 'localStorage', { value: localStorageMock })
    window.localStorage.setItem('user', JSON.stringify({ type: 'Employee', email: "a@a" }))
  })


  describe("When I am on Bills Page", () => {

    test("Then bill icon in vertical layout should be highlighted", async () => {
      const root = document.createElement("div")
      root.setAttribute("id", "root")
      document.body.append(root)
      router()
      window.onNavigate(ROUTES_PATH.Bills)
      await waitFor(() => screen.getByTestId('icon-window'))
      const windowIcon = screen.getByTestId('icon-window')
      expect(windowIcon.classList).toContain('active-icon')
    })
    
    test("Then bills should be ordered from earliest to latest", () => {
      document.body.innerHTML = BillsUI({ data: bills })
      const dates = screen.getAllByText(/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/i).map(a => a.innerHTML)
      const antiChrono = (a, b) => ((a < b) ? 1 : -1)
      const datesSorted = [...dates].sort(antiChrono)
      expect(dates).toEqual(datesSorted)
    })

    test("Then fetch bills from mock API GET", async () => {
      const root = document.createElement('div')
      root.setAttribute('id', 'root')
      document.body.append(root)
      router()
      window.onNavigate(ROUTES_PATH.Bills)

      await waitFor(() => screen.getByText('Mes notes de frais'))
      expect(screen.getByText('Mes notes de frais')).toBeTruthy()
      const bodyTable = screen.getByTestId('tbody')
      expect(bodyTable).toBeTruthy()
      expect(bodyTable.childElementCount).toEqual(4)
    })


    describe("If an error occurs on API", () => {

      beforeEach(() => {
        jest.spyOn(mockStore, "bills")

        const root = document.createElement("div")
        root.setAttribute("id", "root")
        document.body.appendChild(root)
        router()
      })

      test("Then fetch bills from an API and fails with 404 message error", async () => {
        mockStore.bills.mockImplementationOnce(() => {
          return {
            list : () =>  {
              return Promise.reject(new Error('Erreur 404'))
            }
          }
        })

        window.onNavigate(ROUTES_PATH.Bills)
        await new Promise(process.nextTick)
        expect(screen.getByText(/Erreur 404/)).toBeTruthy()
      })

      test("Then fetch messages from an API and fails with 500 message error", async () => {
        mockStore.bills.mockImplementationOnce(() => {
          return {
            list : () =>  {
              return Promise.reject(new Error('Erreur 500'))
            }
          }
        })
  
        window.onNavigate(ROUTES_PATH.Bills)
        await new Promise(process.nextTick)
        expect(screen.getByText(/Erreur 500/)).toBeTruthy()
      })
    })
  })


  describe("When I am on Bills Page and I click on New Bill button", () => {

    test("Then I am redirected to New Bill page", () => {
      const root = document.createElement("div")
      root.setAttribute("id", "root")
      document.body.append(root)
      router()
      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname })
      }

      const billsContainer = new Bills({ document, onNavigate, store: null, localStorage: window.localStorage })

      const handleClickNewBill = jest.spyOn(billsContainer, 'handleClickNewBill')
      const newBillBtn = screen.getByTestId('btn-new-bill')

      userEvent.click(newBillBtn)
      expect(handleClickNewBill).toHaveBeenCalled()
      expect(screen.getByTestId("form-new-bill")).toBeTruthy()
    })
  })


  describe("When I am on Bills Page and I click on eye icon", () => {
    
    test("Then a modal should open", () => {
      const onNavigate = (pathname) => {
        document.body.innerHTML = ROUTES({ pathname })
      }
      document.body.innerHTML = BillsUI({ data: bills })
      const billsContainer = new Bills({ document, onNavigate, store: null, localStorage: window.localStorage })

      const handleClickIconEye = jest.spyOn(billsContainer, 'handleClickIconEye')
      const anyEyeIcon = screen.getAllByTestId('icon-eye')[0]
      $.fn.modal = jest.fn()

      userEvent.click(anyEyeIcon)
      expect(handleClickIconEye).toHaveBeenCalledWith(anyEyeIcon)
      expect($.fn.modal).toHaveBeenCalledWith('show')
    })
  })
})
